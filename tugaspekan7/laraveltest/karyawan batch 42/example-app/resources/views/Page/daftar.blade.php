@extends('layout.master')
@section('title')
    Halaman Biodata
@endsection
@section('content')

<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>
<br><br>
<form action="/kirim" method="post">
    @csrf
    <label for="">First name:</label> <br>
    <input type="text" name="fname"> <br> <br>
    <label for="">Last name:</label> <br>
    <input type="text" name="lname"> <br> <br>
    <label for="">Gender:</label> <br>
    <input type="radio" name="gender"> <label for="">Male</label> <br>
    <input type="radio" name="gender"> <label for="">Female</label><br>
    <input type="radio" name="gender"> <label for="">Other</label><br><br>
    <label for="">Nationality:</label> <br>
    <select name="Nationality" id="">
        <option value="Indonesia">Indonesian</option>
        <option value="Malaysian">Malaysian</option>
        <option value="Brunei">Brunei</option>
        <option value="Thailand">Thailand</option>
    </select> <br><br>
    <label for="">Language Spoken:</label> <br>
    <input type="checkbox" name="Language" id=""><label for="">Bahasa Indonesia</label> <br>
    <input type="checkbox" name="Language" id=""><label for="">English</label> <br>
    <input type="checkbox" name="Language" id=""><label for="">Other</label> <br><br>
    <label for="">Bio</label><br><br>
    <textarea name="" id="" cols="30" rows="10"></textarea><br>
    <input type="submit" value="Sign Up">
</form>
@endsection



