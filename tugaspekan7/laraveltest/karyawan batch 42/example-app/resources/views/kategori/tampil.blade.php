@extends('layout.master')
@section('title')
    Halaman Tampil Kategori
@endsection
@section('content')

<a href="/kategori/create" class="btn btn-primary my-3">Create Data</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
        <th scope="col">Handle</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($kategories as $key => $item)
        <tr>            
        <th scope="row">{{$key + 1}}</th>
        <td>{{$item->nama}}</td>
        <td>
            <form action="/kategori/{{$item->id}}" method="post">
                @csrf
                @method('delete')
                <a href="/kategori/{{$item->id}}" class="btn btn-sm btn-info">Detail</a>
                <a href="/kategori/{{$item->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
            <input type="submit" value="hapus" class="btn btn-sm btn-danger">
            </form>
        </td>
    </tr>
    @empty
            <h1>Tidak Ada Data Kategori</h1>
        @endforelse
      <tr>
        <th scope="row">2</th>
        <td>Jacob</td>
        <td>Thornton</td>
        <td>@fat</td>
      </tr>
      <tr>
        <th scope="row">3</th>
        <td>Larry</td>
        <td>the Bird</td>
        <td>@twitter</td>
      </tr>
    </tbody>
  </table>
@endsection