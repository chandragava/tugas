<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\BiodataController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index']);
Route::get('/daftar', [BiodataController::class, 'daftar']);
Route::post('/kirim', [BiodataController::class, 'kirim']);

Route::get('/data-table', function(){
    return view('page.table');
});

//CRUD

//Create Data
//menuju ke inputan tambah data
Route::get('/kategori/create', [KategoriController::class, 'create']);
//untuk aksi simpan data di database
Route::post('/kategori', [KategoriController::class, 'store']);

//Read Data
//route untuk menampikan semua data
Route::get('/kategori', [KategoriController::class, 'index']);
//route dinamis yang bisa get data dari detail berdasarkan id
Route::get('/kategori/{id}', [KategoriController::class, 'show']);

//update Data
//menuju ke form inputan tambah data berdasarkan id
Route::get('/kategori/{id}/edit', [KategoriController::class, 'edit']);
//update data di db berdasarkan id
Route::put('/kategori/{id}', [KategoriController::class, 'update']);

//Delete Data
Route::delete('/kategori/{id}', [KategoriController::class, 'destroy']);

//CRUD CAST
//menuju ke inputan tambah data
Route::get('/cast/create', [CastController::class, 'create']);

//untuk aksi simpan data di database
Route::post('/cast', [CastController::class, 'store']);

//Read Data
//route untuk menampikan semua data
Route::get('/cast', [CastController::class, 'index']);

//route dinamis yang bisa get data dari detail berdasarkan id
Route::get('/cast/{id}', [CastController::class, 'show']);

//update Data
//menuju ke form inputan tambah data berdasarkan id
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);

//update data di db berdasarkan id
Route::put('/cast/{id}', [CastController::class, 'update']);

//Delete Data
Route::delete('/cast/{id}', [CastController::class, 'destroy']);

