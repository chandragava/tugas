<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kategori;

class KategoriController extends Controller
{
    public function create()
    {
        return view('kategori.tambah');
    }
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required',
        ]);
        Kategori::create([
            'nama' => $request["nama"],
            'deskripsi' => $request["deskripsi"],
        ]);
        
        return redirect('/kategori');
    }

    public function index()
    {
        $kategories = Kategori::all();
        // dd($kategories);
        return view('kategori.tampil', ['kategories' => $kategories]);
    }

    public function show($id)
    {
        $kategori = Kategori::where('id', $id)->first();
        
        return view('kategori.detail', ['kategori'=> $kategori]);
    }

    public function edit($id)
    {
        $kategori = Kategori::where('id', $id)->first();
        
        return view('kategori.edit', ['kategori'=> $kategori]);
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required',
        ]);
        $kategori = Kategori::find($id);
 
        $kategori->nama = $request['nama'];
        $kategori->deskripsi = $request['deskripsi'];
        
        $kategori->save();

        return redirect('/kategori');
    }

    public function destroy($id){
        $kategori = Kategori::find($id);
 
        $kategori->delete();

        return redirect('/kategori');
    }

    
    
}
