<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cast;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.tambah');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'name' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        Cast::create([
            'name' => $request["name"],
            'umur' => $request["umur"],
            'bio' => $request["bio"],
        ]);

        return redirect('/cast');
    }
    public function index()
    {
        $kategories = Cast::all();
        // dd($kategories);
        return view('cast.tampil', ['kategories' => $kategories]);
    }

    public function show($id)
    {
        $cast = Cast::where('id', $id)->first();
        
        return view('cast.detail', ['cast'=> $cast]);
    }

    public function edit($id)
    {
        $cast = Cast::where('id', $id)->first();
        
        return view('cast.edit', ['cast'=> $cast]);
    }

    public function update($id, Request $request){
        $request->validate([
            'name' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $cast = Cast::find($id);
 
        $cast->name = $request['name'];
        $cast->umur = $request['umur'];
        $cast->bio = $request['bio'];
        
        $cast->save();

        return redirect('/cast');
    }
    public function destroy($id){
        $cast = Cast::find($id);
 
        $cast->delete();

        return redirect('/cast');
    }
    
}
