<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\BiodataController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\BeritaController;
use App\Http\Controllers\KomentarController;
use App\Http\Controllers\FilmController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index']);
Route::get('/daftar', [BiodataController::class, 'daftar']);
Route::post('/kirim', [BiodataController::class, 'kirim']);

Route::get('/data-table', function(){
    return view('page.table');
});

//CRUD

Route::group(['middleware' => ['auth']], function () {
    //
    //Create Data
    //menuju ke inputan tambah data
    Route::get('/kategori/create', [KategoriController::class, 'create']);
    //untuk aksi simpan data di database
    Route::post('/kategori', [KategoriController::class, 'store']);
    
    //Read Data
    //route untuk menampikan semua data
    Route::get('/kategori', [KategoriController::class, 'index']);
    //route dinamis yang bisa get data dari detail berdasarkan id
    Route::get('/kategori/{id}', [KategoriController::class, 'show']);
    
    //update Data
    //menuju ke form inputan tambah data berdasarkan id
    Route::get('/kategori/{id}/edit', [KategoriController::class, 'edit']);
    //update data di db berdasarkan id
    Route::put('/kategori/{id}', [KategoriController::class, 'update']);
    
    //Delete Data
    Route::delete('/kategori/{id}', [KategoriController::class, 'destroy']);
    
    //CRUD CAST
    //menuju ke inputan tambah data
    Route::get('/cast/create', [CastController::class, 'create']);
    
    //untuk aksi simpan data di database
    Route::post('/cast', [CastController::class, 'store']);
    
    //Read Data
    //route untuk menampikan semua data
    Route::get('/cast', [CastController::class, 'index']);
    
    //route dinamis yang bisa get data dari detail berdasarkan id
    Route::get('/cast/{id}', [CastController::class, 'show']);
    
    //update Data
    //menuju ke form inputan tambah data berdasarkan id
    Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
    
    //update data di db berdasarkan id
    Route::put('/cast/{id}', [CastController::class, 'update']);
    
    //Delete Data
    Route::delete('/cast/{id}', [CastController::class, 'destroy']);

    Route::post('/komentar/{id}', [KomentarController::Class, 'comment']);
    
    // CRUD GENRE
    // menuju ke inputan tambah data
    Route::get('/genre/create', [GenreController::class, 'create']);
    
    //untuk aksi simpan data di database
    Route::post('/genre', [GenreController::class, 'store']);
    
    //Read Data
    //route untuk menampikan semua data
    Route::get('/genre', [GenreController::class, 'index']);
    
    //route dinamis yang bisa get data dari detail berdasarkan id
    Route::get('/genre/{id}', [GenreController::class, 'show']);
    
    //update Data
    //menuju ke form inputan tambah data berdasarkan id
    Route::get('/genre/{id}/edit', [GenreController::class, 'edit']);
    
    //update data di db berdasarkan id
    Route::put('/genre/{id}', [GenreController::class, 'update']);
    
    //Delete Data
    Route::delete('/genre/{id}', [GenreController::class, 'destroy']);
    
    // CRUD FILM
    // menuju ke inputan tambah data
    Route::get('/film/create', [FilmController::class, 'create']);
    
    //untuk aksi simpan data di database
    Route::post('/film', [FilmController::class, 'store']);
    
    //Read Data
    //route untuk menampikan semua data
    Route::get('/film', [FilmController::class, 'index']);
    
    //route dinamis yang bisa get data dari detail berdasarkan id
    Route::get('/film/{id}', [FilmController::class, 'show']);
    
    //update Data
    //menuju ke form inputan tambah data berdasarkan id
    Route::get('/film/{id}/edit', [FilmController::class, 'edit']);
    
    //update data di db berdasarkan id
    Route::put('/film/{id}', [FilmController::class, 'update']);
    
    //Delete Data
    Route::delete('/film/{id}', [FilmController::class, 'destroy']);
});



// CRUD POST
Route::resource('post', PostController::class);

// CRUD BERITA
Route::get('/berita/create', [BeritaController::class, 'create']);

//untuk aksi simpan data di database
Route::post('/berita', [BeritaController::class, 'store']);

//Read Data
//route untuk menampikan semua data
Route::get('/berita', [BeritaController::class, 'index']);

//route dinamis yang bisa get data dari detail berdasarkan id
Route::get('/berita/{id}', [BeritaController::class, 'show']);

//update Data
//menuju ke form inputan tambah data berdasarkan id
Route::get('/berita/{id}/edit', [BeritaController::class, 'edit']);

//update data di db berdasarkan id
Route::put('/berita/{id}', [BeritaController::class, 'update']);

//Delete Data
Route::delete('/berita/{id}', [BeritaController::class, 'destroy']);



Auth::routes();
