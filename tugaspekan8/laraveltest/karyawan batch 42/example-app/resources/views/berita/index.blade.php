@auth

<a href="/berita/create" class="btn btn-primary my-2">Tambah</a>
@endauth

<div class="row">
    @forelse ($berita as $item)
    
    <div class="col-4">
        <div class="card">

            <a href="/berita/{{$item->id}}">
                <img src="{{asset('image/'). $item->image}}" style="height: 200px" class="card-img-top" alt="...">
            </a>
            <div class="card-body">
                <h5>{{$item->judul}}</h5>
                <span class="badge badge-secondary">{{$item->kategori->nama}}</span>
                <p class="card-text">{{ Str::limit($item->content, 20) }}</p>

                <a href="/berita/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                @auth
                <form action="/berita/{{$item->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/berita/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" value="Delete class=btn btn-danger btn-sm">
                </form>
                @endauth
            </div>
        </div>
    </div>
        
    @empty
        <h1>Tidak Ada Berita</h1>
    @endforelse

</div>

@endsection