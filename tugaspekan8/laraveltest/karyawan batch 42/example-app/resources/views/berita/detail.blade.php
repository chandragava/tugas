@extends('layout.master')
@section('title')
    Halaman Detail Berita
@endsection
@section('content')

<h1>{{$berita->judul}}</h1>
<h1>{{$berita->content}}</h1>
<p>{{$berita->image}}</p>
<p>{{$berita->kategori_id}}</p>

<img src="{{asset('image/'). $berita->image}}" style="width: 100vh; height: 400px" class="card-img-top" alt="...">
    <h2>{{$item->judul}}</h2>
    <p class="card-text">{{$berita->content, 20}}</p>
<hr>

<h3>List Komentar</h3>
@forelse ($berita->komentar as $item)
<div class="card">
    <div class="card-header">
      {{$item->user->names}}
    </div>
    <div class="card-body">
        <p class="card-text">{{$item->content}}</p>
     
    </div>
  </div>
    
@empty
    <h1>Tidak ada komenar</h1>
@endforelse

<hr>

<form action="/komentar/{{$berita->id}}" class="my-5" method="post">
@csrf
<textarea name="content" id="" class="form-control my-2" cols="30" rows="10" placeholder="Isi Komentar"></textarea>

@error('content')
<div class="alert alert-danger" role="alert">
    {{$message}}
  </div>
<input type="submit" class="btn btn-primary btn-sm" value="Kirim">
</form>

<hr>
@endsection