@extends('layout.master')
@section('title')
    Halaman Edit Berita
@endsection
@section('content')

<form action="/berita/{{$berita->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label >Judul Berita</label>
      <input type="text" value="{{$berita->judul}}" name="judul" class="form-control" >
          </div>
          @error('judul')
      <div class="alert alert-danger">{{ $message }}</div>
          @enderror
<div class="form-group">
  <label >Content Berita</label>
  <input type="text" value="{{$berita>content}}" name="content" class="form-control" >
  </div>
  @error('content')
<div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
      <label >Image Berita</label>
      <textarea name="image" class="form-control" cols="30" rows="10">{{$berita->image}}</textarea>
    </div>
    @error('image')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group">
      <label >Kategori Berita</label>
      <textarea name="kategori_id" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('kategori_id')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection