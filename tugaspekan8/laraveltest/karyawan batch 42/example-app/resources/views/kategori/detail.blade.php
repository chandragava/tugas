@extends('layout.master')
@section('title')
    Halaman Detail Kategori
@endsection
@section('content')

<h1>{{$kategori->nama}}</h1>
<p>{{$kategori->deskripsi}}</p>

<div class="row">
@forelse ($kategori->berita as $item)
    <div class="col-4">
        <div class="card">

            <a href="/berita/{{$item->id}}">
                <img src="{{asset('image/'). $item->image}}" style="height: 200px" class="card-img-top" alt="...">
            </a>
            <div class="card-body">
                <h5>{{$item->judul}}</h5>
                <p class="card-text">{{ Str::limit($item->content, 20) }}</p>
            </div>
        </div>
    </div>
    @empty
    <h2>Tidak ada berita di kategori ini</h2>
    @endforelse
</div>

@endsection