@extends('layout.master')
@section('title')
    Halaman Edit Film
@endsection
@section('content')

<form action="/film/{{$film->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label >Judul Film</label>
      <input type="text" value="{{$film->nama}}" name="judul" class="form-control" >
          </div>
          @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
      <label >Ringkasan Film</label>
      <textarea name="ringkasan" class="form-control" cols="30" rows="10">{{$film->ringkasan}}</textarea>
    </div>
    @error('ringkasan')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group">
    <label >Tahun Film</label>
    <textarea name="tahun" class="form-control" cols="30" rows="10">{{$film->tahun}}</textarea>
  </div>
  @error('tahun')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label >Poster Film</label>
    <textarea name="poster" class="form-control" cols="30" rows="10">{{$film->poster}}</textarea>
  </div>
  @error('poster')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label >Genre Film</label>
    <textarea name="genre_id" class="form-control" cols="30" rows="10">{{$film->genre_id}}</textarea>
  </div>
  @error('genre_id')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection