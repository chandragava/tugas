@extends('layout.master')
@section('title')
    Halaman Tambah Film
@endsection
@section('content')

<form action="/film" method="POST">
    @csrf
    <div class="form-group">
      <label >Judul Film </label>
      <input type="text" name="judul" class="form-control" >
          </div>
          @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
      <label >Ringkasan Film</label>
      <input type="text" name="ringkasan" class="form-control" >
      </div>
      @error('ringkasan')
<div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
      <label >Tahun Film</label>
      <textarea name="tahun" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group">
    <label >Poster Film</label>
    <textarea name="poster" class="form-control" cols="30" rows="10"></textarea>
  </div>
  @error('poster')
  <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group">
    <label >Genre Film</label>
    <textarea name="genre_id" class="form-control" cols="30" rows="10"></textarea>
  </div>
  @error('genre_id')
  <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection