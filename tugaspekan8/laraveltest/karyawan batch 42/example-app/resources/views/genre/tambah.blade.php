@extends('layout.master')
@section('title')
    Halaman Tambah Genre
@endsection
@section('content')

<form action="/genre" method="POST">
    @csrf
    <div class="form-group">
      <label >Name Kategori</label>
      <input type="text" name="name" class="form-control" >
          </div>
          @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
      <label >Deskripsi Kategori</label>
      <textarea name="deskripsi" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('deskripsi')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection