@extends('layout.master')
@section('title')
    Halaman Tampil Genre
@endsection
@section('content')

<a href="/genre/create" class="btn btn-primary my-3">Create Data</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($kategories as $key => $item)
        <tr>            
        <th scope="row">{{$key + 1}}</th>
        <td>{{$item->name}}</td>
        <td>
            <form action="/genre/{{$item->id}}" method="post">
                @csrf
                @method('delete')
                <a href="/genre/{{$item->id}}" class="btn btn-sm btn-info">Detail</a>
                <a href="/genre/{{$item->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
            <input type="submit" value="hapus" class="btn btn-sm btn-danger">
            </form>
        </td>
    </tr>
    @empty
            <h1>Tidak Ada Data Kategori</h1>
        @endforelse
      
  </table>
@endsection