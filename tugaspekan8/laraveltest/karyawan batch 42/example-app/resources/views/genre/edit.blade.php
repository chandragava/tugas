@extends('layout.master')
@section('title')
    Halaman Edit Genre
@endsection
@section('content')

<form action="/genre/{{$genre->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label >Nama Kategori</label>
      <input type="text" value="{{$genre->nama}}" name="name" class="form-control" >
          </div>
          @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
      <label >Deskripsi Kategori</label>
      <textarea name="deskripsi" class="form-control" cols="30" rows="10">{{$genre->deskripsi}}</textarea>
    </div>
    @error('deskripsi')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection