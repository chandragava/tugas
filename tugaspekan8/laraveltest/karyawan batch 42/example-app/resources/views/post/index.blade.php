@extends('layout.master')
@section('title')
    Halaman List Post
@endsection
@section('content')



<a href="/post/create" class="btn btn-primary btn-sm mb-4">Tambah Post</a>

<div class="row">
    @forelse ($post as $item)
    <div class="col-4">

        <div class="card" >
        <img src="{{asset('image/'. $item->thumbnail)}}" class="card-img-top" alt="...">
        <div class="card-body">
          <h5>{{$item->judul}}</h5>
          <p class="card-text"> {{ Str::limit($item->content, 50) }}</p>
          <a href="/post/{{$item->id}}" class="btn btn-secondary btn=block btn-sm">Read Me</a>
          <div class="row my-2">
            <div class="col">
                <a href="/post/{{$item->id}}/edit" class="btn btn-info btn=block btn-sm">Edit</a>
            </div>
            <div class="col">
                <form action="post/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger btn-block btn-sm" value="delete">

                </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    @empty
    <h2>Tidak ada Postingan</h2>
    @endforelse
    
</div>
@endsection