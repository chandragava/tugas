@extends('layout.master')
@section('title')
    Halaman Detail Post
@endsection
@section('content')

    <img src="{{asset('image/'. $post->thumbnail)}}" class="card-img-top" alt="...">
      <h5>{{$post->judul}}</h5>
      <p class="card-text">{{$post->content}}</p>
      <a href="/post" class="btn btn-secondary btn=block btn-sm">Kembali</a>
    
@endsection
