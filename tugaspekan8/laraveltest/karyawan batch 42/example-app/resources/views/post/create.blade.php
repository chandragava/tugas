@extends('layout.master')
@section('title')
    Halaman Tambah Post
@endsection
@section('content')

<form action="/post" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label >Judul</label>
      <input type="text" name="judul" class="form-control" >
          </div>
          @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="form-group">
      <label >Content Post</label>
      <textarea name="content" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('content')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group">
    <label >Thumbnail</label>
    <input type="file" name="thumbnail" class="form-control" >
        </div>
        @error('thumbnail')
  <div class="alert alert-danger">{{ $message }}</div>
@enderror

<div class="form-group">
    <label >Kategori</label>
    <select name="kategori_id" class="form-control" id="">
        <option value="">--pilih kategori--</option>
        @forelse ($kategori as $item)
            <option value="{{$item->id}}">{{$item->nama}}</option>
        @empty
            <option value="">Tidak ada Data Kategori</option>
        @endforelse
    </select>
        </div>
        @error('thumbnail')
  <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection