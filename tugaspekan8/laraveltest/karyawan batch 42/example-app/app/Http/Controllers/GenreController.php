<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Genre;
class GenreController extends Controller
{
    public function create()
    {
        return view('genre.tambah');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'name' => 'required',
            'deskripsi' => 'required',
        ]);
        Genre::create([
            'name' => $request["name"],
            'deskripsi' => $request["deskripsi"],
        ]);
        
        return redirect('/genre');
    }

    public function index()
    {
        $kategories = Genre::all();
        // dd($kategories);
        return view('genre.tampil', ['kategories' => $kategories]);
    }

    public function show($id)
    {
        $genre = Genre::where('id', $id)->first();
        
        return view('genre.detail', ['genre'=> $genre]);
    }

    public function edit($id)
    {
        $genre = Genre::where('id', $id)->first();
        
        return view('genre.edit', ['genre'=> $genre]);
    }

    public function update($id, Request $request){
        $request->validate([
            'name' => 'required',
            'deskripsi' => 'required',
        ]);
        $genre = Genre::find($id);
 
        $genre->name = $request['name'];
        $genre->deskripsi = $request['deskripsi'];
        
        $genre->save();

        return redirect('/genre');
    }
    public function destroy($id)
    {
        $genre = Genre::find($id);
 
        $genre->delete();

        return redirect('/genre');
    }

    
}
