<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Film;

class FilmController extends Controller
{
    public function create()
    {
        return view('film.tambah');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required',
            'genre_id' => 'required',
        ]);
        Film::create([
            'judul' => $request["judul"],
            'ringkasan' => $request["ringkasan"],
            'tahun' => $request["tahun"],
            'poster' => $request["poster"],
            'genre_id' => $request["genre_id"],
        ]);
        
        return redirect('/film');
    }

    public function index()
    {
        $kategories = Film::all();
        // dd($kategories);
        return view('film.tampil', ['kategories' => $kategories]);
    }

    public function show($id)
    {
        $film = Film::where('id', $id)->first();
        
        return view('film.detail', ['film'=> $film]);
    }

    public function edit($id)
    {
        $film = Film::where('id', $id)->first();
        
        return view('film.edit', ['film'=> $film]);
    }
    public function update($id, Request $request){
        $request->validate([
            'judul' => $request["judul"],
            'ringkasan' => $request["ringkasan"],
            'tahun' => $request["tahun"],
            'poster' => $request["poster"],
            'genre_id' => $request["genre_id"],
        ]);
        $film = Film::find($id);
 
        $film->judul = $request['judul'];
        $film->ringkasan = $request['ringkasan'];
        $film->tahun = $request['tahun'];
        $film->poster = $request['poster'];
        $film->genre_id = $request['genre_id'];
        
        $film->save();

        return redirect('/film');
    }

    public function destroy($id)
    {
        $film = Film::find($id);
 
        $film->delete();

        return redirect('/film');
    }
}
