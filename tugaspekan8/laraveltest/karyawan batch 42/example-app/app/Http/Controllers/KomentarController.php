<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Komentar;
use Illuminate\Support\Facades\Auth;


class KomentarController extends Controller
{
    public function comment($id, Request $request)
    {
        $request->validate([
            'content' => 'required',
     
        ]);

        $iduser = Auth::id();



        $komentar = new Komentar;
 
        $komentar->content = $request('content');
        $komentar->content = $id;
        $komentar->user_id = $iduser;
 
        $komentar->save();

        return redirect('/berita/' . $id);

    }
}
