<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Berita;

class BeritaController extends Controller
{
    public function create()
    {
        return view('berita.tambah');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'judul' => 'required',
            'content' => 'required',
            'image' => 'required',
            'kategori_id' => 'required',
        ]);

        Berita::create([
            'judul' => $request["judul"],
            'content' => $request["content"],
            'image' => $request["image"],
            'kategori_id' => $request["kategori_id"],
        ]);

        return redirect('/berita');
    }

    public function index()
    {
        $kategories = Berita::all();
        // dd($kategories);
        return view('berita.tampil', ['kategories' => $kategories]);
    }

    public function show($id)
    {
        $berita = Berita::where('id', $id)->first();
        
        return view('berita.detail', ['berita'=> $berita]);
    }

    public function edit($id)
    {
        $berita = Berita::where('id', $id)->first();
        
        return view('berita.edit', ['berita'=> $berita]);
    }

    public function update($id, Request $request){
        $request->validate([
            'judul' => 'required',
            'content' => 'required',
            'image' => 'required',
            'kategori_id' => 'required',
        ]);
        $berita = Berita::find($id);
 
        $berita->judul = $request['judul'];
        $berita->content = $request['content'];
        $berita->image = $request['image'];
        $berita->kategori_id = $request['kategori_id'];
        
        $berita->save();

        return redirect('/berita');
    }
    public function destroy($id){
        $berita = Berita::find($id);
 
        $berita->delete();

        return redirect('/berita');
    }
}
