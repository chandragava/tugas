<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kategori;
use App\Models\Post;
use File;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $post = Post::get();
        return view('post.index', ['post' => $post]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $kategori = Kategori::get();
        return view('post.create', ['kategori' => $kategori]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'content' => 'required',
            'kategori_id' => 'required',
            'thumbnail' => 'required|image|mimes:jpg,png,jpeg'
        ]);

        $filename = time().'.'.$request->thumbnail->extension();

        $request->thumbnail->move(public_path('image'), $filename);

        $post = new Post;
 
        $post->judul = $request->judul;
        $post->content = $request->content;
        $post->kategori_id = $request->kategori_id;
        $post->thumbnail = $filename;
 
        $post->save();

        return redirect('/post');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $post = Post::find($id);
    
        return view('post.detail', ['post'=> $post]);
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $post = Post::find($id);
        $kategori = Kategori::get();

        return view('post.update', ['post'=> $post, 'kategori' => $kategori]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'judul' => 'required',
            'content' => 'required',
            'kategori_id' => 'required',
            'thumbnail' => '|image|mimes:jpg,png,jpeg'
        ]);

        $post = Post::find($id);

        if($request->has('thumbnail')){
            $path = 'image/';
            File::delete($path. $post->thumbnail);

            $filename = time().'.'.$request->thumbnail->extension();

            $request->thumbnail->move(public_path('image'), $filename);

            $post->thumbnail = $filename;

            $post->save();

        }
 
        $post->judul = $request['judul'];
        $post->content = $request['content'];
        $post->kategori_id = $request['kategori_id']; 
        $post->save();

        return redirect('/post');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $post = Post::find($id);
            $path = 'image/';
            File::delete($path. $post->thumbnail);
 
        $post->delete();

        return redirect ('/post');

    }
}
