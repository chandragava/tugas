<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("Shaun");

echo "Name = " . $sheep->merk . "<br>";
echo "Legs = " . $sheep->kaki . "<br>";
echo $sheep->berdarah() . "<br>";

$kodok = new frog("buduk");
echo "Name = " . $kodok->merk . "<br>";
echo "Legs = " . $kodok->kaki . "<br>";
echo $kodok->berdarah();
echo $kodok->Jump() . "<br> <br>";

$sungokong = new Ape("kera sakti");
echo "Name = " . $sungokong->merk . "<br>";
echo "Legs = " . $sungokong->kaki2 . "<br>";
echo $sungokong->berdarah();
echo $sungokong->yell();


?>